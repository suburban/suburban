function A = oneDGridAdjacencyMatrix(M)
A = zeros(M,M) ~= 0;
for i=2:M
    A(i-1,i) = true;
    A(i,i-1) = true;
end
end