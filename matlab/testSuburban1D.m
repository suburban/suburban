% Compares various samplers for a very simple 1D target distribution
%
% In startup.m, add the path where the Java suburban code lives...
% javaaddpath(path-string);
%
function testSuburban1D()

M = 16;             % Number of parallel instances (must be an exact square if using the 2D grid)
N = 1000;           % Number of samples
alpha = 0.1;        % Proposal precision used for MH only
alpha0 = 0.;        % Additive baseline value for alpha when computed adaptively
beta = 0.01;        % Connectivity strength constant
pJoin = 0.5;                    % For string/grid versions - connection probability
joinPeriod = 1;                 % For string/grid versions - period in scans
connectionProbability = 0.08;   % For random-graph version
resamplerPeriod = 1;            % For random-graph version

initRange = 100;                % Initialize values over +/-initRange
initCenter = 0;                 % Center of initialization range
randomScheduler = true;         % Use random-scan Gibbs scheduler or sequential-scan Gibbs scheduler
repeatable = false;             % Whether or not to randomize the initial seed
plotYlim = 200;                 % Range of sample values to plot when zoomed out
plotYlimZoom = 2;               % Range of sample values to plot when zoomed in

% Seed for randomization
if (repeatable)
    seed = 1;
    rs=RandStream('mt19937ar');
    RandStream.setGlobalStream(rs);
    reset(rs,seed);
else
    seed = randi(intmax);
end

% Initial data used for all samplers
vInit = rand(1,M)*2*initRange - initRange + initCenter;


% *******************************************************
% Run each sampler, resuling in an array of sample values
% *******************************************************
vs1d = testWithSuburban1DGrid(M,N,vInit,alpha0,beta,randomScheduler,seed,pJoin,joinPeriod);
vs2d = testWithSuburban2DGrid(M,N,vInit,alpha0,beta,randomScheduler,seed,pJoin,joinPeriod);
vsrg = testWithSuburbanRandomGraph(M,N,vInit,alpha0,beta,randomScheduler,seed,connectionProbability,resamplerPeriod);
vms = testWithMH(M,N,vInit,alpha,randomScheduler,seed);
vss = testWithSliceSampler(M,N,vInit,randomScheduler,seed);


% ********************************************
% Plots
% ********************************************
figure(1);
plotxy = {6,2};
plotNum = 1;

subplot(plotxy{:},plotNum);
NC = (1:N)';
hold off;
plot([0 N],[0 0],'r');
hold on;
plot(abs(cumsum(mean(vs1d,2))./NC),'b');
plot(abs(cumsum(mean(vs2d,2))./NC),'g');
plot(abs(cumsum(mean(vsrg,2))./NC),'c');
plot(abs(cumsum(mean(vms,2))./NC),'m');
plot(abs(cumsum(mean(vss,2))./NC),'k');
ylim([-5,20])
hold off;
title('Distance to mean');
plotNum = plotNum+1;

plotNum = plotNum+1;
subplot(plotxy{:},plotNum);
plot(vs1d,'.');
ylim([-plotYlim plotYlim]);
title('Suburban 1D Grid');
plotNum = plotNum+1;
subplot(plotxy{:},plotNum);
plot(vs1d,'.');
ylim([-plotYlimZoom plotYlimZoom]);
title('Suburban 1D Grid');

plotNum = plotNum+1;
subplot(plotxy{:},plotNum);
plot(vs2d,'.');
ylim([-plotYlim plotYlim]);
title('Suburban 2D Grid');
plotNum = plotNum+1;
subplot(plotxy{:},plotNum);
plot(vs2d,'.');
ylim([-plotYlimZoom plotYlimZoom]);
title('Suburban 2D Grid');

plotNum = plotNum+1;
subplot(plotxy{:},plotNum);
plot(vsrg,'.');
ylim([-plotYlim plotYlim]);
title('Suburban Random Graph');
plotNum = plotNum+1;
subplot(plotxy{:},plotNum);
plot(vsrg,'.');
ylim([-plotYlimZoom plotYlimZoom]);
title('Suburban Random Graph');

plotNum = plotNum+1;
subplot(plotxy{:},plotNum);
plot(vms,'.');
ylim([-plotYlim plotYlim]);
title('MH');
plotNum = plotNum+1;
subplot(plotxy{:},plotNum);
plot(vms,'.');
ylim([-plotYlimZoom plotYlimZoom]);
title('MH');

plotNum = plotNum+1;
subplot(plotxy{:},plotNum);
plot(vss,'.');
ylim([-plotYlim plotYlim]);
title('Slice sampler');
plotNum = plotNum+1;
subplot(plotxy{:},plotNum);
plot(vss,'.');
ylim([-plotYlimZoom plotYlimZoom]);
title('Slice sampler');

end


% ************************************************
% Run with Suburban sampler with 1D grid
% ************************************************
function vs = testWithSuburban1DGrid(M,N,vInit,alpha0,beta,randomScheduler,seed,connectionProbability,resamplePeriod)
import com.analog.lyric.dimple.solvers.gibbs.samplers.block.BlockMHSampler;
import suburbs.*;

[fg, v] = createModelString(M);

fg.Solver = SuburbanSolver;
fg.Solver.setNumSamples(N);
fg.Solver.saveAllSamples();
fg.Solver.setSeed(seed);

if (randomScheduler)
    fg.Scheduler = 'GibbsRandomScanScheduler';
end

% baseAdjacencyMatrix = oneDGridAdjacencyMatrix(M);  % Variant with open string
baseAdjacencyMatrix = oneDLoopAdjacencyMatrix(M);    % Variant with closed string
v.invokeSolverMethod('setSampler','MHSampler');
randomString = RandomStringSplitJoinShuffle(baseAdjacencyMatrix,connectionProbability);
fg.Solver.setRandomString(randomString);
fg.Solver.setStringResamplePeriodInSamples(resamplePeriod);
% vString = SuburbanGeneralizedStringScalar(v.Solver,randomString,alpha0,beta);  % Variant with fixed alpha
vString = SuburbanGeneralizedStringScalarAdaptive(v.Solver,randomString,alpha0,beta);
for m=1:M
    v(m).Solver.getSampler().setProposalKernel(SuburbanGeneralProposalKernel(vString, m-1));
    v(m).Solver.setInitialSampleValue(vInit(m));
end

tic;
fg.solve();
toc;
vs = cell2mat(v.invokeSolverMethodWithReturnValue('getAllSamples'));
fprintf('Suburban 1D grid rejection rate: %f\n', fg.Solver.getRejectionRate());
end

% ************************************************
% Run with Suburban sampler with 2D grid
% ************************************************
function vs = testWithSuburban2DGrid(M,N,vInit,alpha0,beta,randomScheduler,seed,connectionProbability,resamplePeriod)
import com.analog.lyric.dimple.solvers.gibbs.samplers.block.BlockMHSampler;

import suburbs.*;

[fg, v] = createModelString(M);

fg.Solver = SuburbanSolver;
fg.Solver.setNumSamples(N);
fg.Solver.saveAllSamples();
fg.Solver.setSeed(seed);

if (randomScheduler)
    fg.Scheduler = 'GibbsRandomScanScheduler';
end

% baseAdjacencyMatrix = twoDGridAdjacencyMatrix(M);  % Variant with open brane
baseAdjacencyMatrix = twoDTorusAdjacencyMatrix(M);   % Variant with closed torus
v.invokeSolverMethod('setSampler','MHSampler');
randomString = RandomStringSplitJoinShuffle(baseAdjacencyMatrix,connectionProbability);
fg.Solver.setRandomString(randomString);
fg.Solver.setStringResamplePeriodInSamples(resamplePeriod);
% vString = SuburbanGeneralizedStringScalar(v.Solver,randomString,alpha0,beta);  % Variant with fixed alpha
vString = SuburbanGeneralizedStringScalarAdaptive(v.Solver,randomString,alpha0,beta);
for m=1:M
    v(m).Solver.getSampler().setProposalKernel(SuburbanGeneralProposalKernel(vString, m-1));
    v(m).Solver.setInitialSampleValue(vInit(m));
end

tic;
fg.solve();
toc;
vs = cell2mat(v.invokeSolverMethodWithReturnValue('getAllSamples'));
fprintf('Suburban 2D grid rejection rate: %f\n', fg.Solver.getRejectionRate());
end

% ************************************************
% Run with Suburban sampler with random graph
% ************************************************
function vs = testWithSuburbanRandomGraph(M,N,vInit,alpha0,beta,randomScheduler,seed,connectionProbability,resamplePeriod)
import com.analog.lyric.dimple.solvers.gibbs.samplers.block.BlockMHSampler;

import suburbs.*;

[fg, v] = createModelString(M);

fg.Solver = SuburbanSolver;
fg.Solver.setNumSamples(N);
fg.Solver.saveAllSamples();
fg.Solver.setSeed(seed);

if (randomScheduler)
    fg.Scheduler = 'GibbsRandomScanScheduler';
end

v.invokeSolverMethod('setSampler','MHSampler');
randomString = RandomStringErdosRenyiGraph(M,connectionProbability);
fg.Solver.setRandomString(randomString);
fg.Solver.setStringResamplePeriodInSamples(resamplePeriod);
% vString = SuburbanGeneralizedStringScalar(v.Solver,randomString,alpha0,beta);  % Variant with fixed alpha
vString = SuburbanGeneralizedStringScalarAdaptive(v.Solver,randomString,alpha0,beta);
for m=1:M
    v(m).Solver.getSampler().setProposalKernel(SuburbanGeneralProposalKernel(vString, m-1));
    v(m).Solver.setInitialSampleValue(vInit(m));
end

tic;
fg.solve();
toc;
vs = cell2mat(v.invokeSolverMethodWithReturnValue('getAllSamples'));
fprintf('Suburban random-graph rejection rate: %f\n', fg.Solver.getRejectionRate());
end


% ********************************************
% Run with regular MH
% ********************************************
function vs = testWithMH(M,N,vInit,alpha,randomScheduler,seed)
[fg, v] = createModelString(M);

fg.Solver = 'Gibbs';
fg.Solver.setNumSamples(N);
fg.Solver.saveAllSamples();
fg.Solver.setSeed(seed);
if (randomScheduler)
    fg.Scheduler = 'GibbsRandomScanScheduler';
end

propsalStd = 1/sqrt(2*alpha);
v.invokeSolverMethod('setSampler','MHSampler');
for m=1:M
    v(m).Solver.getSampler.getProposalKernel.setStandardDeviation(propsalStd);
    v(m).Solver.setInitialSampleValue(vInit(m));
end

tic;
fg.solve();
toc;
vs = cell2mat(v.invokeSolverMethodWithReturnValue('getAllSamples'));
fprintf('MH rejection rate: %f\n', fg.Solver.getRejectionRate());
end


% ********************************************
% Run with slice sampler
% ********************************************
function vs = testWithSliceSampler(M,N,vInit,randomScheduler,seed)
[fg, v] = createModelString(M);

fg.Solver = 'Gibbs';
fg.Solver.setNumSamples(N);
fg.Solver.saveAllSamples();
fg.Solver.setSeed(seed);
if (randomScheduler)
    fg.Scheduler = 'GibbsRandomScanScheduler';
end

for m=1:M
    v(m).Solver.setInitialSampleValue(vInit(m));
end

tic;
fg.solve();
toc;
vs = cell2mat(v.invokeSolverMethodWithReturnValue('getAllSamples'));
end






% ********************************************
% Create 1D model
% ********************************************
function [fg, v] = createModelString(M)
import suburbs.*;

% One copy of the model
[sg, sv] = createModel();
sg.addBoundaryVariables(sv);

% Repeat the model M times
fg = FactorGraph;
v = Real(1,M);
fg.addFactorVectorized(sg, v);

end


% Single instance of the target model
function [fg, v] = createModel()
import suburbs.*;

fg = FactorGraph();
v = Real;

% Normal mixture
fg.addFactorVectorized(TestNormalMixture1D(1, 10, -0.5, 10), v);

% % Rectangular mixture
% fg.addFactorVectorized(TestRectangularMixture1D(0.5, 0.5, -0.5, 0.5, 10000, 1), v);

end