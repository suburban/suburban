% Runs the joint suburban sampler for a multi-dimensional target distribution
%
% In startup.m, add the path where the Java suburban code lives...
% javaaddpath(path-string);
%
function testSuburbanDdimJoint()
global means weights widths numMix plotJointDistribution plotYlimZoom

D = 2;              % Number of dimensions in target distribution
M = 100;            % Number of parallel instances (must be an exact square if using the 2D grid)
N = 10000;          % Number of samples
alpha0 = 0.;        % Additive baseline value for alpha when computed adaptively
beta = 0.01;        % Connectivity strength constant
pJoin = 0.8;                    % For string/grid versions - connection probability
joinPeriod = 1;                 % For string/grid versions - period in scans

initRange = 100;                % Initialize values over +/-initRange
initCenter = 0;                 % Center of initialization range
randomScheduler = true;         % Use random-scan Gibbs scheduler or sequential-scan Gibbs scheduler
repeatable = false;             % Whether or not to randomize the initial seed
plotYlim = 200;                 % Range of sample values to plot when zoomed out
plotYlimZoom = 10;              % Range of sample values to plot when zoomed in
plotJointDistribution = true;   % Works only when D = 2

% Seed for randomization
if (repeatable)
    seed = 1;
    rs=RandStream('mt19937ar');
    RandStream.setGlobalStream(rs);
    reset(rs,seed);
else
    seed = randi(intmax);
end

% Initial data used for all samplers
vInit = rand(D,M)*2*initRange - initRange + initCenter;


% Parameters for target distribution (specified here so they'll be in
% common for all runs of the samplers)...

% % Random mixture pattern (for either Normal or rectangular)
% meansRange = 10;
% numMix = 3;
% means = rand(numMix,D) * meansRange - meansRange/2;
% weights = rand(1,numMix);
% widths = 0.2 + rand(numMix,D);

% Quadrant mixture pattern (for either Normal or rectangular)
numMix = 2 * D;
weights = ones(1,numMix) / numMix;
means_plus = (+ 1.5) * eye(D,D); % half of the vertices
means_mins = (- 1.5) * eye(D,D); % other half of the vertices
means = cat(1, means_plus , means_mins); % and the total list of means...
widths = ones(numMix,D);


% *******************************************************
% Run each sampler (well, just the 1D grid for now)
% *******************************************************
vs1d = testWithSuburban1DGrid(M,N,D,vInit,alpha0,beta,randomScheduler,seed,pJoin,joinPeriod);


% ********************************************
% Plots
% ********************************************
figure(1);
hold off;
plotxy = {1,2};
plotNum = 1;

subplot(plotxy{:},plotNum);
myPlot(vs1d,'.');
ylim([-plotYlim plotYlim]);
zlim([-plotYlim plotYlim]);
title('Suburban 1D grid');
plotNum = plotNum+1;
subplot(plotxy{:},plotNum);
myPlot(vs1d,'.');
ylim([-plotYlimZoom plotYlimZoom]);
zlim([-plotYlimZoom plotYlimZoom]);
title('Suburban 1D grid');

end


% ************************************************
% Run with Suburban sampler with 1D grid
% ************************************************
function [vs,sMean,sCov,sTail,sTotal] = testWithSuburban1DGrid(M,N,D,vInit,alpha0,beta,randomScheduler,seed,connectionProbability,resamplePeriod)
import com.analog.lyric.dimple.solvers.gibbs.samplers.block.BlockMHSampler;

import suburbs.*;

[fg, v] = createModel(M,D);

fg.Solver = SuburbanSolver;
fg.Solver.setNumSamples(N);
fg.Solver.saveAllSamples();
fg.Solver.setSeed(seed);

fg.Solver.getJointStatistics.setTailThresholds([1.5,2.0,2.5]);
fg.Solver.getJointStatistics.setStartDelay(1000);


if (randomScheduler)
    fg.Scheduler = 'GibbsRandomScanScheduler';
end

% baseAdjacencyMatrix = oneDGridAdjacencyMatrix(M);  % Variant with open string
baseAdjacencyMatrix = oneDLoopAdjacencyMatrix(M);    % Variant with closed string
v.invokeSolverMethod('setSampler','JointMHSampler');
sv = v.Solver;
randomString = RandomStringSplitJoinShuffle(baseAdjacencyMatrix,connectionProbability,false);
fg.Solver.setRandomString(randomString);
fg.Solver.setStringResamplePeriodInSamples(resamplePeriod);
vString = SuburbanGeneralizedStringJointAdaptive(sv,randomString,alpha0,beta);
for m=1:M
    v(m).Solver.getSampler().setProposalKernel(SuburbanGeneralProposalKernel(vString, m-1));
    v(m).Solver.setInitialSampleValue(vInit(:,m));
end
for m=1:M
    fg.Solver.getJointStatistics.addJointVariable(sv(m));
end

tic;
fg.solve();
toc;
vs = convert2mat(v.invokeSolverMethodWithReturnValue('getAllSamples'));

% Get the resulting statistics
sMean = fg.Solver.getJointStatistics.getSampleMean();
sCov = fg.Solver.getJointStatistics.getSampleCovariance();
sTail = fg.Solver.getJointStatistics.getTailThresholdCounts();
sTotal = fg.Solver.getJointStatistics.getSampleCount();

fprintf('Suburban 1D grid rejection rate: %f\n', fg.Solver.getRejectionRate());
end



function m = convert2mat(c)
sca = size(c);
scm = size(c{1});
sm = [scm sca];
sm = sm(sm > 1);
m = reshape(cell2mat(c), sm);
end

% values is N x D x M
function myPlot(values, varargin)
hold off;
N = size(values,1);
D = size(values,2);
M = size(values,3);
for m=1:M
    vm = squeeze(values(:,:,m));
    color = rand(1,3);
    plot3(1:N,vm(:,1),vm(:,2),varargin{:}, 'Color', color);
    hold on;
end

end


% **************************************************************
% Create D-dimensional model
% **************************************************************
function [fg, v] = createModel(M,D)
global means weights widths numMix plotJointDistribution plotYlimZoom
import suburbs.*;

fg = FactorGraph;
v = RealJoint(D,1,M);

% % % Normal mixture
% mixStd = 0.5;
% informationMatrices = zeros(numMix,D,D);
% for n=1:numMix
%     informationMatrices(n,:,:) = eye(D,D)/mixStd^2;
% end
% ff = TestNormalMixtureJoint(means, informationMatrices, weights);

% % Normal
% ff = FactorFunction('MultivariateNormal', zeros(1,D), cov);
% ff = ff.IFactorFunction;

% % Rectangular mixture
% ff = TestRectangularMixtureJoint(means, widths, 10000, 0);

% Banana distribution
ff = TestBananaPairwiseJoint(1, 100);

for i=1:M
    fg.addFactor(ff, v(i));
end

% Plot the distribution (assumes D = 2)
if (plotJointDistribution && D == 2)
    figure(2);
    N=100; x=linspace(-plotYlimZoom,plotYlimZoom,N); [xx,yy]=meshgrid(x,x); z=zeros(N,N); for i=1:N; for j=1:N; z(i,j) = ff.eval({[x(i),x(j)]}); end;end; surf(xx,yy,z);
end
end
