function A = twoDTorusAdjacencyMatrix(M)

A = zeros(M,M) ~= 0;
dx = [-1 0 1 0];
dy = [0 -1 0 1];
sqrtM = round(sqrt(M));
index = 1;
for y=1:sqrtM
    for x=1:sqrtM
        for edge=1:4
            xNeighbor = mod(x-1 + dx(edge),sqrtM)+1;
            yNeighbor = mod(y-1 + dy(edge),sqrtM)+1;
            neighborIndex = ((xNeighbor-1) + (yNeighbor-1) * sqrtM) + 1;
            A(index,neighborIndex) = true;
        end
        index = index + 1;
    end
end
end