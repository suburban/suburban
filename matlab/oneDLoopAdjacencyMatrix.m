function A = oneDLoopAdjacencyMatrix(M)
A = zeros(M,M) ~= 0;
for i=1:M
    A(i,mod(i,M)+1) = true;
    A(mod(i,M)+1,i) = true;
end
end