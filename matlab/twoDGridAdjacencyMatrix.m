function A = twoDGridAdjacencyMatrix(M)

A = zeros(M,M) ~= 0;
dx = [-1 0 1 0];
dy = [0 -1 0 1];
sqrtM = round(sqrt(M));
index = 1;
for y=1:sqrtM
    for x=1:sqrtM
        for edge=1:4
            xNeighbor = x + dx(edge);
            yNeighbor = y + dy(edge);
            neighborIndex = ((xNeighbor-1) + (yNeighbor-1) * sqrtM) + 1;
            if (xNeighbor > 0 && yNeighbor > 0 && xNeighbor <= sqrtM && yNeighbor <= sqrtM)
                A(index,neighborIndex) = true;
            end
        end
        index = index + 1;
    end
end
end