This code supplements the paper "MCMC with Strings and Branes: The Suburban 
Algorithm" by Heckman, et al.  This project includes Java extensions to the 
Dimple probabilistic programming library to support running the suburban 
algorithm as described in the paper.  It also includes a few short MATLAB 
scripts illustrating simple examples using various forms of the suburban 
sampler.

COPYRIGHT
=========

Copyright 2016 Analog Devices, Inc. and Jonathan Heckman.
This project is licensed under the Apache License, Version 2.0.
See NOTICE.txt and LICENSE.txt.

JAVA LIBRARIES
==============

The Java libraries in the /java folder depend on release 0.07.1 of the Dimple 
probabilistic programming library, which exists in the Branch "release_0.07.1" 
(https://github.com/AnalogDevicesLyricLabs/dimple/tree/release_0.07.1).  Note 
that the suburban library is not compatible with the master branch of Dimple, 
nor any release other than release_0.07.1.  Instructions for installing the 
Dimple library can be found at 
http://dimple.probprog.org/documentation/installation.

To support using the Java libraries (including the Dimple libraries) via 
MATLAB, you'll need to ensure that the Java libraries are complied using Java 7 
compatibility (for example, in the Eclipse IDE, in the Java Compiler settings, 
set the Compiler Compliance Level to 1.7).

The Dimple software is a probabilistic programming library that allows defining 
probabilistic models as factor graphs, and supports a variety of solvers to 
perform inference.  The Gibbs solver in particular performs inference via Gibbs 
sampling, and supports several types of samplers for individual one-dimensional 
or multi-dimensional variables.  This includes conjugate samplers for certain 
specific distributions as well as Metropolis-Hastings sampling and slice 
sampling for distributions for which no conjugate sampler is available.

The suburban Java library includes an extension of Dimple's Gibbs solver that 
implements aspects of the suburban algorithm.  Specifically, it allows coupling 
of samplers to form strings and branes, as described in the paper.  This 
includes connectivity with a specified adjacency matrix, such as a 1D string, 
2D brane, etc., as well as various random graphs.  In all cases, these allow 
the forms of randomized connectivity described in the paper.  The library also 
includes an augmentation of the Metropolis-Hastings sampler in Dimple to 
include the currently connected neighbors in forming the proposal, as described 
in the paper.  And finally, the library includes some very simple parameterized 
target distributions that were used for testing and evaluation of the 
algorithm, though suburban is by no means limited to using just these 
distributions.

One thing the current implementation does not do is explicitly implement the 
replication of the target distribution into a product of M copies of that 
distribution.  Instead, the graphical model must explicitly replicate all 
variables and factors, forming a model larger than the original target.  The 
API includes methods to indicate which copies of variables to associated with a 
single string/brane.  A more practical implementation of the suburban algorithm 
would have hidden this complexity, allowing any model defined as a factor graph 
to be used without modification.  Such an implementation is left for a future 
effort.

EXAMPLE SCRIPTS
===============

This project also includes a few MATLAB scripts that show some simple examples 
that use the suburban algorithm.  These are in the /matlab folder.  Note that 
these are not the scripts that were used to generate the results in the paper, 
but instead simply demonstrate how to use the suburban libraries and show some 
simple comparisons between variations of the suburban algorithm and other 
samplers.  The following scripts are provided:

- testSuburban1D : Compares various samplers for a very simple 1D target 
        distribution

- testSuburbanDdim : Compares various samplers for a multi-dimensional target 
        distribution

- testSuburbanDdimJoint : Runs the joint suburban sampler for a 
        multi-dimensional target distribution
