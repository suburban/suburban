package suburbs;

import com.analog.lyric.dimple.exceptions.DimpleException;
import com.analog.lyric.math.DimpleRandomGenerator;

public class RandomStringSplitJoinShuffle extends AbstractRandomString
{
	protected final StringGraph _baseGraph;
	private final double _connectionProbability;
	private final boolean _shuffle;
	
	public RandomStringSplitJoinShuffle(boolean[][] baseAdjacencyMatrix, double connectionProbability)
	{
		this(baseAdjacencyMatrix, connectionProbability, true);	// Shuffle by default
	}
	public RandomStringSplitJoinShuffle(boolean[][] baseAdjacencyMatrix, double connectionProbability, boolean shuffle)
	{
		super(baseAdjacencyMatrix.length);
		_connectionProbability = connectionProbability;
		_shuffle = shuffle;
		int size = baseAdjacencyMatrix.length;
		_baseGraph = new StringGraph(size);
		for (int i = 0; i < size; i++)
		{
			boolean[] row = baseAdjacencyMatrix[i];
			if (row.length != size)
				throw new DimpleException("Invalid adjacency matrix");
			for (int j = i + 1; j < size; j++)
			{
				if (row[j])
					_baseGraph.addEdge(i, j);
			}
		}
		resample();	// Generate initial graph
	}
	public RandomStringSplitJoinShuffle(StringGraph baseGraph, double connectionProbability)
	{
		this(baseGraph, connectionProbability, true);	// Shuffle by default
	}
	public RandomStringSplitJoinShuffle(StringGraph baseGraph, double connectionProbability, boolean shuffle)
	{
		super(baseGraph.getNumNodes());
		_baseGraph = baseGraph;
		_connectionProbability = connectionProbability;
		_shuffle = shuffle;
	}


	@Override
	public void resample()
	{
		final StringGraph stringGraph = _stringGraph;
		final int numNodes = stringGraph.getNumNodes();
		if (numNodes != _baseGraph.getNumNodes())
			throw new DimpleException("Incorrect number of elements");
		stringGraph.clear();

		final double connectionProbability = _connectionProbability;
		for (StringGraph.Edge edge : _baseGraph.getEdges())
		{
			final double U = DimpleRandomGenerator.rand.nextDouble();
			if (U <= connectionProbability)
				stringGraph.addEdge(edge);
		}
		
		if (_shuffle)
			stringGraph.reorderNodes();
		
		super.postResample();
	}

}
