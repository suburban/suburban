package suburbs;


import com.analog.lyric.dimple.exceptions.DimpleException;
import com.analog.lyric.dimple.factorfunctions.core.FactorFunction;
import com.analog.lyric.dimple.model.values.Value;


public class TestBananaDdim extends FactorFunction
{
	protected double[] _centers;
	protected double[] _invwidths;
	protected double[] _x;
	protected int _dimension;

	public TestBananaDdim(double[] centers, double[] invwidths)
	{
		super();
		_centers = centers;  // where you put the center of the gaussian part: (x - 1)^2
		_invwidths = invwidths; // the thing that controls the quartic term:  (x - y^2)^2
		_dimension = centers.length;
		_x = new double[_dimension];
	}
	
    @Override
    public final double evalEnergy(Value[] arguments)
    {
    	if (arguments.length != _dimension)
    		throw new DimpleException("Number of dimensions must equal the number of elements in the parameter vectors");

		for (int i = 0; i < _dimension; i++)
		{
			_x[i] = arguments[i].getDouble();		// Input vector
		}

		double energy = 0;
		
		final int dm1 = _dimension - 1;
    	for (int dcount = 0; dcount < dm1; dcount++) // compute the energy as a sum of terms...
    	{
    		final double xd = _x[dcount];
    		final double firstdiff = xd - _centers[dcount];
    		final double secondiff = _x[dcount + 1] - xd * xd;
    		final double firstterm = firstdiff * firstdiff + _invwidths[dcount] * (secondiff * secondiff);
    		energy += firstterm;
    	}
    	
    	return energy;  // eat the banana!
    }
}
