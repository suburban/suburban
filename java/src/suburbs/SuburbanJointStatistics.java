package suburbs;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.analog.lyric.dimple.exceptions.DimpleException;
import com.analog.lyric.dimple.solvers.gibbs.GibbsReal;
import com.analog.lyric.dimple.solvers.gibbs.GibbsRealJoint;

public class SuburbanJointStatistics
{
	private IVariableList _jointVariables;
	private double[] _sum = null;
	private double[][] _sumSq = null;
	private long[] _tailThresholdCounts;
	private double[] _tailThresholdCenter = null;
	private double[] _tailThresholds;
	private long _updateCount;
	private long _totalUpdateCount;
	private long _startDelay = 0;

	public void initialize()
	{
		_updateCount = 0;
		_totalUpdateCount = 0;
		if (_sum != null)
			Arrays.fill(_sum, 0);
		if (_sumSq != null)
			for (double[] row : _sumSq)
				Arrays.fill(row, 0);
	}
	
	public final void addJointVariable(Object[] variables)
	{
		if (_jointVariables == null)
		{
			if (variables[0] instanceof GibbsRealJoint)
				_jointVariables = new RealJointVariableList();
			else
				_jointVariables = new RealVariableList();
		}
		
		_jointVariables.addVariable(variables);
		
		final int dimension = _jointVariables.getDimension();
		if (_sum == null)
			_sum = new double[dimension];
		if (_sumSq == null)
			_sumSq = new double[dimension][dimension];
	}
	public final void clearJointVariables()
	{
		if (_jointVariables != null)
			_jointVariables.clear();
		_sum = null;
		_sumSq = null;
		_updateCount = 0;
	}
	
	public final void setTailThresholds(double[] tailThresholdList)
	{
		_tailThresholds = tailThresholdList;
		_tailThresholdCounts = new long[tailThresholdList.length];
	}
	
	public final void setTailThresholdCenter(double[] tailThresholdCenter)
	{
		_tailThresholdCenter = tailThresholdCenter;
	}
	
	public final void setStartDelay(long delay)
	{
		_startDelay = delay;
	}
	
	public final void updateStatistics()
	{
		_totalUpdateCount++;
		if (_totalUpdateCount <= _startDelay)
			return;
		
		if (_jointVariables == null)
			return;
		
		// Update the sums for computing moments
		final int numSets = _jointVariables.size();
		for (int set = 0; set < numSets; set++)
		{
			final int dimension = _jointVariables.getDimension();
			final double[] sampleValue = _jointVariables.getCurrentSample(set);
			
			double maxAbsDifference = 0;
			for (int i = 0; i < dimension; i++)
			{
				// Compute stats for mean and covariance
				final double sampleValueI = sampleValue[i];
				final double[] sumSqI = _sumSq[i];
				_sum[i] += sampleValueI;
				for (int j = i; j < dimension; j++)
					sumSqI[j] += sampleValueI * sampleValue[j];
				
				// Compute stats for tail threshold counts (L-infinity distance from center)
				final double centerValue = (_tailThresholdCenter != null) ? _tailThresholdCenter[i] : 0;
				final double absDifferenceFromCenter = Math.abs(sampleValueI - centerValue);
				if (absDifferenceFromCenter > maxAbsDifference)
					maxAbsDifference = absDifferenceFromCenter;
			}
			
			// Update the tail threshold counts
			final int numThresholds = _tailThresholds.length;
			for (int iThreshold = 0; iThreshold < numThresholds; iThreshold++)
				if (maxAbsDifference > _tailThresholds[iThreshold])
					_tailThresholdCounts[iThreshold]++;
			
			_updateCount++;
		}
	}
	
	
	public final double[] getSampleMean()
	{
		if (_jointVariables != null && !_jointVariables.isEmpty())
		{
			final int length = _sum.length;
			final double[] mean = new double[length];
			final double updateCount = _updateCount;
			for (int i = 0; i < length; i++)
				mean[i] = _sum[i] / updateCount;
			return mean;
		}
		else
		{
			throw new DimpleException("The sample mean is only computed if there have been joint varaibles added");
		}
	}
	
	public final double[][] getSampleCovariance()
	{
		if (_jointVariables != null && !_jointVariables.isEmpty())
		{
			// For all sample values, compute the covariance matrix
			// For now, use the naive algorithm; could be improved
			final int length = _sumSq.length;
			final double[][] covariance = new double[length][length];
			final double updateCount = _updateCount;
			final double updateCountMinusOne = (updateCount - 1);
			for (int i = 0; i < length; i++)
			{
				for (int j = i; j < length; j++)
				{
					final double sumi = _sum[i];
					final double sumj = _sum[j];
					final double sumSqij = _sumSq[i][j];
					final double value = (sumSqij - sumi * (sumj / updateCount) ) / updateCountMinusOne;
					covariance[i][j] = value;
					covariance[j][i] = value;	// Fill in lower triangular half
				}
			}
			return covariance;
		}
		else
		{
			throw new DimpleException("The sample covariance is only computed if there have been joint varaibles added");
		}
	}
	
	public final long[] getTailThresholdCounts()
	{
		return _tailThresholdCounts;
	}
	
	public final long getSampleCount()
	{
		return _updateCount;
	}
	
	
	
	
	private interface IVariableList
	{
		public void addVariable(Object[] variable);
		public double[] getCurrentSample(int variableIndex);
		public void clear();
		public boolean isEmpty();
		public int getDimension();
		public int size();
	}
	private class RealVariableList implements IVariableList
	{
		private List<GibbsReal[]> _jointVariables;

		@Override
		public void addVariable(Object[] variable)
		{
			if (_jointVariables == null)
				_jointVariables = new ArrayList<GibbsReal[]>();
			
			GibbsReal[] sVariables = new GibbsReal[variable.length];
			for (int i = 0; i < variable.length; i++)
				sVariables[i] = (GibbsReal)variable[i];
			_jointVariables.add(sVariables);
		}

		@Override
		public double[] getCurrentSample(int variableIndex)
		{
			final GibbsReal[] variable = _jointVariables.get(variableIndex);
			final double[] out = new double[variable.length];
			for (int i = 0; i < variable.length; i++)
				out[i] = variable[i].getCurrentSample();
			return out;
		}
		
		@Override
		public void clear()
		{
			_jointVariables.clear();
		}

		@Override
		public boolean isEmpty()
		{
			return _jointVariables == null || _jointVariables.isEmpty();
		}
		
		@Override
		public int getDimension()
		{
			if (_jointVariables == null)
				return -1;
			else
				return _jointVariables.get(0).length;		
		}
		
		@Override
		public int size()
		{
			return _jointVariables.size();
		}
	}
	private class RealJointVariableList implements IVariableList
	{
		private List<GibbsRealJoint> _jointVariables;

		@Override
		public void addVariable(Object[] variable)
		{
			final GibbsRealJoint variables = (GibbsRealJoint)(variable[0]);
			if (_jointVariables == null)
				_jointVariables = new ArrayList<GibbsRealJoint>();
			
			_jointVariables.add(variables);
		}

		@Override
		public double[] getCurrentSample(int variableIndex)
		{
			final GibbsRealJoint variable = _jointVariables.get(variableIndex);
			return variable.getCurrentSample();
		}

		@Override
		public void clear()
		{
			_jointVariables.clear();
		}

		@Override
		public boolean isEmpty()
		{
			return _jointVariables == null || _jointVariables.isEmpty();
		}
		
		@Override
		public int getDimension()
		{
			if (_jointVariables == null)
				return -1;
			else
				return _jointVariables.get(0).getDimension();		
		}
		
		@Override
		public int size()
		{
			return _jointVariables.size();
		}
	}
}
