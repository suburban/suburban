package suburbs;

import com.analog.lyric.math.DimpleRandomGenerator;

public class RandomStringErdosRenyiGraph extends AbstractRandomString
{
	private final double _connectionProbability;
	
	public RandomStringErdosRenyiGraph(int numNodes, double connectionProbability)
	{
		super(numNodes);
		_connectionProbability = connectionProbability;
		resample();	// Generate initial graph
	}


	@Override
	public void resample()
	{
		final StringGraph stringGraph = _stringGraph;
		final int numNodes = stringGraph.getNumNodes();
		stringGraph.clear();

		final double connectionProbability = _connectionProbability;
		for (int i = 0; i < numNodes; i++)
		{
			for (int j = i + 1; j < numNodes; j++)
			{
				final double U = DimpleRandomGenerator.rand.nextDouble();
				if (U <= connectionProbability)
					stringGraph.addEdge(i, j);
			}
		}
		
		super.postResample();
	}

}
