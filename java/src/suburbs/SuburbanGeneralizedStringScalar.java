package suburbs;


import com.analog.lyric.dimple.model.domains.Domain;
import com.analog.lyric.dimple.model.values.Value;
import com.analog.lyric.dimple.solvers.core.proposalKernels.Proposal;
import com.analog.lyric.dimple.solvers.gibbs.GibbsReal;
import com.analog.lyric.math.DimpleRandomGenerator;

public class SuburbanGeneralizedStringScalar implements ISuburbanString
{
	private final GibbsReal[] _variables;
	private final IString _string;
	private final double _alpha;
	private final double _beta;
	private double[] _neighborValues;
	private static final double _logSqrt2pi = Math.log(2*Math.PI)*0.5;


	public SuburbanGeneralizedStringScalar(Object[] variables, IString string, double alpha, double beta)
	{
		_string = string;
		_alpha = alpha;
		_beta = beta;

		_neighborValues = new double[variables.length];
		
		_variables = new GibbsReal[variables.length];
		for (int i = 0; i < variables.length; i++)
			_variables[i] = (GibbsReal)variables[i];
	}
	
	public Proposal getProposal(int updateIndex, Value currentValue, Domain variableDomain)
	{		
		final int neighborCount = _string.getNeighborCount(updateIndex);
		final double alpha = _alpha;
		final double beta = _beta;
		final double precisionOverTwo = alpha + beta * neighborCount;
		final double meanNormalizationConstant = 1/precisionOverTwo;
		final double precision = 2*precisionOverTwo;
		final double sigma = 1/Math.sqrt(precision);
		final double logSqrtPrecisionOver2Pi = Math.log(precision)*0.5 - _logSqrt2pi;

		// Extract the current values
		final double current = currentValue.getDouble();
		{
			int n = 0;
			for (Integer neighbor : _string.getNeighbors(updateIndex))
				_neighborValues[n++] = _variables[neighbor].getCurrentSample();
		}

		// Compute the pre-proposal mean
		double preMeanSum = alpha * current;
		for (int n = 0; n < neighborCount; n++)
			preMeanSum += beta * _neighborValues[n];
		final double preMean = preMeanSum * meanNormalizationConstant;

		// Generate the proposal to replace the current value
		final double proposal = DimpleRandomGenerator.rand.nextGaussian() * sigma + preMean;

		// Compute the post-proposal mean
		double postMeanSum = alpha * proposal;
		for (int n = 0; n < neighborCount; n++)
			postMeanSum += beta * _neighborValues[n];
		final double postMean = postMeanSum * meanNormalizationConstant;

		// Compute the Hastings term
		final double proposalReverseEnergy = -logQ(current, postMean, precisionOverTwo, logSqrtPrecisionOver2Pi);
		final double proposalForwardEnergy = -logQ(proposal, preMean, precisionOverTwo, logSqrtPrecisionOver2Pi);

		return new Proposal(proposal, proposalForwardEnergy, proposalReverseEnergy);
	}

	private final double logQ(double x, double mean, double precisionOverTwo, double logSqrtPrecisionOver2Pi)
	{
		double xRel = x - mean;
		return logSqrtPrecisionOver2Pi - (xRel * xRel) * precisionOverTwo;
	}
	
}
