package suburbs;

import com.analog.lyric.dimple.factorfunctions.core.FactorFunction;
import com.analog.lyric.dimple.model.values.Value;


public class TestRectangularMixture1D extends FactorFunction
{
	private double _threshold_0;
	private double _threshold_1;
	private double _threshold_2;
	private double _threshold_3;
	private double _highEnergy;
	private double _rampRate;

	public TestRectangularMixture1D(double mean_0, double width_0, double mean_1, double width_1)
	{
		this(mean_0, width_0, mean_1, width_1, 10000, 0);
	}
	public TestRectangularMixture1D(double mean_0, double width_0, double mean_1, double width_1, double highEnergy)
	{
		this(mean_0, width_0, mean_1, width_1, highEnergy, 0);
	}
	public TestRectangularMixture1D(double mean_0, double width_0, double mean_1, double width_1, double highEnergy, double rampRate)
	{
		super();
		if (mean_0 < mean_1)
		{
			_threshold_0 = mean_0 - width_0/2;
			_threshold_1 = mean_0 + width_0/2;
			_threshold_2 = mean_1 - width_1/2;
			_threshold_3 = mean_1 + width_1/2;
		}
		else
		{
			_threshold_0 = mean_1 - width_1/2;
			_threshold_1 = mean_1 + width_1/2;
			_threshold_2 = mean_0 - width_0/2;
			_threshold_3 = mean_0 + width_0/2;
		}
		_highEnergy = highEnergy;
		_rampRate = rampRate;
	}
	
    @Override
    public final double evalEnergy(Value[] arguments)
    {
    	final double x = arguments[0].getDouble();
    	
    	double e;
    	if (x > _threshold_3)
    		e = _highEnergy + (x - _threshold_3)*_rampRate;
    	else if (x < _threshold_3 && x > _threshold_2)
    		e = 0;
    	else if (x < _threshold_2 && x > _threshold_1)
    		e = _highEnergy;
    	else if (x < _threshold_1 && x > _threshold_0)
    		e = 0;
    	else
    		e = _highEnergy + (_threshold_0 - x)*_rampRate;

    	return e;
    }
}
