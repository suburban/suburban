package suburbs;

import com.analog.lyric.dimple.model.core.FactorGraph;
import com.analog.lyric.dimple.solvers.gibbs.GibbsSolverGraph;

public class SuburbanSolverGraph extends GibbsSolverGraph
{
	private IRandomString _string;
	private int _stringResamplePeriodInSamples = 1;
	private int _stringResampleCount = 0;
	private SuburbanJointStatistics _jointStatitics;
	
	protected SuburbanSolverGraph(FactorGraph factorGraph)
	{
		super(factorGraph);
		
		_jointStatitics = new SuburbanJointStatistics();
	}
	
	public final void setRandomString(IRandomString string)
	{
		_string = string;
	}
	
	public final void setStringResamplePeriodInSamples(int stringResamplePeriodInSamples)
	{
		_stringResamplePeriodInSamples = stringResamplePeriodInSamples;
	}
	
	@Override
	public void iterate(int numUpdates)
	{
		super.iterate(numUpdates);
		
		// Compute statistics
		if (numUpdates > 0)
			_jointStatitics.updateStatistics();
		
		// Resample the string after each Gibbs sample
		if (_stringResamplePeriodInSamples > 0 && ++_stringResampleCount >= _stringResamplePeriodInSamples)
		{
			_stringResampleCount = 0;
			_string.resample();
		}
	}
	
	@Override
	public void initialize()
	{
		super.initialize();
		_stringResampleCount = 0;
		if (_string != null)
			_string.resample();
		_jointStatitics.initialize();
	}

	
	public SuburbanJointStatistics getJointStatistics()
	{
		return _jointStatitics;
	}
	
}
