package suburbs;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.math3.distribution.PoissonDistribution;

import com.analog.lyric.dimple.exceptions.DimpleException;
import com.analog.lyric.math.DimpleRandomGenerator;

public class RandomStringReddyOrbanzGraph extends AbstractRandomString
{
	private final double _alpha;
	private final PoissonDistribution _poisson;
	
	// For debug
	private int _restartWalkSameLocationExceptionCount = 0;
	private int _restartWalkNewLocationExceptionCount = 0;
	private int _restartOuterLoopExceptionCount = 0;
	private int _resampleCountForExceptions = 0;
	

	public RandomStringReddyOrbanzGraph(int numNodes, double alpha, double lambda)
	{
		super(numNodes);
		if (alpha <= 0 || alpha > 1)
			throw new DimpleException("Alpha must be greater than zero and not more than one");
		if (lambda <= 0)
			throw new DimpleException("Lambda must be greater than zero");
		_alpha = alpha;
		_poisson = new PoissonDistribution(DimpleRandomGenerator.rand, lambda, PoissonDistribution.DEFAULT_EPSILON, PoissonDistribution.DEFAULT_MAX_ITERATIONS);
		resample();	// Generate initial graph
	}


	@Override
	public void resample()
	{
		_resampleCountForExceptions++;
		
		final StringGraph stringGraph = _stringGraph;
		final int numNodes = stringGraph.getNumNodes();
		stringGraph.clear();

		final double alpha = _alpha;
		ArrayList<Integer> connectedNodes = new ArrayList<Integer>();
		ArrayList<Integer> unconnectedNodes = new ArrayList<Integer>();
		for (int i = 0; i < numNodes; i++)
			unconnectedNodes.add(i);
		
		// Start with two random initial nodes connected
		final int firstNode = unconnectedNodes.remove(DimpleRandomGenerator.rand.nextInt(unconnectedNodes.size()));
		connectedNodes.add(firstNode);
		final int secondNode = unconnectedNodes.remove(DimpleRandomGenerator.rand.nextInt(unconnectedNodes.size()));
		connectedNodes.add(secondNode);
		stringGraph.addEdge(firstNode, secondNode);

		boolean done = false;
		while (!done)
		{
			int node = connectedNodes.get(DimpleRandomGenerator.rand.nextInt(connectedNodes.size()));
			final double U = DimpleRandomGenerator.rand.nextDouble();
			if (U <= alpha)
			{
				// Bernoulli(alpha) = 1, so add an unconnected node
				// But if there are no more nodes to add, then we're done
				if (unconnectedNodes.size() == 0)
					done = true;
				else
				{
					// Add an unconnected node
					final int nextNode = unconnectedNodes.remove(DimpleRandomGenerator.rand.nextInt(unconnectedNodes.size()));
					stringGraph.addEdge(node, nextNode);
					connectedNodes.add(nextNode);
				}
			}
			else
			{
				// Bernoulli(alpha) = 0, so do a random walk to form a new edge
				while (true)
				{
					// First, check if this node is already connected to all other nodes already in the graph
					if (stringGraph.getNeighborCount(node) >= connectedNodes.size() - 1)
					{
						// Node is already connected to all other nodes
						// Then check if all nodes are fully connected
						// If so, go back to the beginning so a new node can be attached
						if (isFullyConnected(stringGraph, connectedNodes))
						{
							_restartOuterLoopExceptionCount++;
							break;
						}
						else
						{
							// Not fully connected, so choose another node in the graph and try the walk again
							node = connectedNodes.get(DimpleRandomGenerator.rand.nextInt(connectedNodes.size()));
							_restartWalkNewLocationExceptionCount++;
							continue;
						}
					}
					
					// Do the random walk
					final int k = _poisson.sample() + 1;	// Choose the number of steps to walk
					int walkNode = node;					// Start the walk at the current node
					for (int i = 0; i < k; i++)
					{
						final int numNeighbors = stringGraph.getNeighborCount(walkNode);
						final Set<Integer> neighbors = stringGraph.getNeighbors(walkNode);
						int randNeighbor = DimpleRandomGenerator.rand.nextInt(numNeighbors);
						int n = 0;
						for (Integer neighbor : neighbors)
						{
							if (n++ >= randNeighbor)
							{
								walkNode = neighbor.intValue();
								break;
							}
						}
					}
					
					// Check if walk went back to current node or to an already connected node
					if (node == walkNode || stringGraph.getNeighbors(node).contains(walkNode))
					{
						_restartWalkSameLocationExceptionCount++;
						continue;	// Landed at current node or already connected node, so then try the walk again
					}
					else
					{
						// Not already connected, so make a new connection and continue
						stringGraph.addEdge(node, walkNode);
						break;
					}
				}
			}
		}
		
		super.postResample();
	}

	
	private boolean isFullyConnected(StringGraph graph, List<Integer> connectedNodes)
	{
		final int numConnectedNodes = connectedNodes.size();
		final int numFullyConnectedNeighbors = numConnectedNodes - 1;
		for (int i = 0; i < numConnectedNodes; i++)
		{
			if (graph.getNeighborCount(connectedNodes.get(i)) < numFullyConnectedNeighbors)
				return false;
		}
		return true;
	}
	
	
	// For debug
	public double getRestartWalkSameLocationExceptions() {return (double)_restartWalkSameLocationExceptionCount/(double)_resampleCountForExceptions;}
	public double getRestartWalkNewLocationExceptions() {return (double)_restartWalkNewLocationExceptionCount/(double)_resampleCountForExceptions;}
	public double getRestartOuterLoopExceptions() {return (double)_restartOuterLoopExceptionCount/(double)_resampleCountForExceptions;}
	public int getResampleCount() {return _resampleCountForExceptions;}
	public void resetExceptionStatistics()
	{
		_restartWalkSameLocationExceptionCount = 0;
		_restartWalkNewLocationExceptionCount = 0;
		_restartOuterLoopExceptionCount = 0;
		_resampleCountForExceptions = 0;
	}
}
