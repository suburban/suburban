package suburbs.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import suburbs.RandomStringSplitJoinShuffle;
import suburbs.StringGraph;

import com.analog.lyric.math.DimpleRandomGenerator;

public class TestRandomStringSplitJoinShuffle
{

	@Test
	public void testSmall()
	{
		DimpleRandomGenerator.setSeed(11);

		int numNodes = 4;
		boolean[][] adj = new boolean[][]
				{{ false, true, true, false },
				{ true, false, false, true },
				{ true, false, false, true },
				{ false, true, true, false }};
		{
			double p = 1;
			RandomStringSplitJoinShuffle g = new RandomStringSplitJoinShuffle(adj, p);
			assertEquals(g.getNumNodes(), numNodes);
			for (int i = 0; i < numNodes; i++)
				assertEquals(g.getNeighborCount(i), 2);
			g.resample();
			assertEquals(g.getNumNodes(), numNodes);
			for (int i = 0; i < numNodes; i++)
				assertEquals(g.getNeighborCount(i), 2);
		}
		{
			double p = 0;
			RandomStringSplitJoinShuffle g = new RandomStringSplitJoinShuffle(adj, p);
			assertEquals(g.getNumNodes(), numNodes);
			for (int i = 0; i < numNodes; i++)
				assertEquals(g.getNeighborCount(i), 0);
			g.resample();
			assertEquals(g.getNumNodes(), numNodes);
			for (int i = 0; i < numNodes; i++)
				assertEquals(g.getNeighborCount(i), 0);
		}
		{
			double p = 0.5;
			RandomStringSplitJoinShuffle g = new RandomStringSplitJoinShuffle(adj, p, false); // Non-shuffling version
			assertEquals(g.getNumNodes(), numNodes);
			for (int i = 0; i < numNodes; i++)
				for (Integer n : g.getNeighbors(i))
					assert(adj[i][n]);	// Make sure neighbors are all in base graph
			g.resample();
			assertEquals(g.getNumNodes(), numNodes);
			for (int i = 0; i < numNodes; i++)
				for (Integer n : g.getNeighbors(i))
					assert(adj[i][n]);	// Make sure neighbors are all in base graph
		}
	}

	@Test
	public void testLarge()
	{
		DimpleRandomGenerator.setSeed(12);

		int numNodes = 100;
		boolean[][] adj = new boolean[numNodes][numNodes];
		for (int i = 0; i < numNodes; i++)
		{
			for (int j = i+1; j < numNodes; j++)
			{
				adj[i][j] = DimpleRandomGenerator.rand.nextBoolean();
				adj[j][i] = adj[i][j];
			}
		}
		
		{
			double p = 0.5;
			RandomStringSplitJoinShuffle g = new RandomStringSplitJoinShuffle(adj, p, false); // Non-shuffling version

			for (int resample = 0; resample < 10; resample++)
			{
				assertEquals(g.getNumNodes(), numNodes);
				for (int i = 0; i < numNodes; i++)
					for (Integer n : g.getNeighbors(i))
						assert(adj[i][n]); // Make sure neighbors are all in base graph
				g.resample();
			}
		}

		{
			double p = 0;
			for (int run = 0; run < 10; run++)
			{
				p += 0.1;
				MyRandomStringSplitJoinShuffle g = new MyRandomStringSplitJoinShuffle(adj, p);

				for (int resample = 0; resample < 10; resample++)
				{
					int numEdges = g.getCurrentGraph().getNumEdges();
					int numBaseEdges = g.getBaseGraph().getNumEdges();
					double expectedEdges = numBaseEdges * p;
					double expectedStdDev = Math.sqrt(numBaseEdges * p * (1-p));
					assert(Math.abs(numEdges - expectedEdges) < expectedStdDev*3);

					g.resample();
				}
			}
		}
	}


	private static class MyRandomStringSplitJoinShuffle extends RandomStringSplitJoinShuffle
	{

		public MyRandomStringSplitJoinShuffle(boolean[][] baseAdjacencyMatrix, double connectionProbability, boolean shuffle)
		{
			super(baseAdjacencyMatrix, connectionProbability, shuffle);
		}

		public MyRandomStringSplitJoinShuffle(boolean[][] baseAdjacencyMatrix, double connectionProbability)
		{
			super(baseAdjacencyMatrix, connectionProbability);
		}

		public MyRandomStringSplitJoinShuffle(StringGraph baseGraph, double connectionProbability, boolean shuffle)
		{
			super(baseGraph, connectionProbability, shuffle);
		}

		public MyRandomStringSplitJoinShuffle(StringGraph baseGraph, double connectionProbability)
		{
			super(baseGraph, connectionProbability);
		}

		public StringGraph getCurrentGraph()
		{
			return _stringGraph;
		}

		public StringGraph getBaseGraph()
		{
			return _baseGraph;
		}
	}
}
