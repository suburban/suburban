package suburbs.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import suburbs.RandomStringReddyOrbanzGraph;

import com.analog.lyric.math.DimpleRandomGenerator;

public class TestRandomStringReddyOrbanzGraph
{

	@Test
	public void test()
	{
		DimpleRandomGenerator.setSeed(12);

		{
			// Simple small graph that should be a tree (alpha = 1)
			int numNodes = 4;
			double alpha = 1;
			double lambda = 1;
			RandomStringReddyOrbanzGraph g = new RandomStringReddyOrbanzGraph(numNodes, alpha, lambda);
			assertEquals(g.getNumNodes(), numNodes);
			for (int i = 0; i < numNodes; i++)
				assert(g.getNeighborCount(i) > 0 || g.getNeighborCount(i) <= numNodes);
			g.resample();
			assertEquals(g.getNumNodes(), numNodes);
			for (int i = 0; i < numNodes; i++)
				assert(g.getNeighborCount(i) > 0 || g.getNeighborCount(i) <= numNodes);
		}
		
		{
			// Random-size graph that should be a tree (alpha = 1)
			for (int i = 0; i < 100; i++)
			{
				int numNodes = DimpleRandomGenerator.rand.nextInt(100) + 5;
				double alpha = 1;
				double lambda = 1;
				RandomStringReddyOrbanzGraph g = new RandomStringReddyOrbanzGraph(numNodes, alpha, lambda);
				assertEquals(g.getNumNodes(), numNodes);
				assertEquals(g.getNumEdges(), numNodes-1);
				g.resample();
				assertEquals(g.getNumNodes(), numNodes);
				assertEquals(g.getNumEdges(), numNodes-1);
			}
		}

		{
			int numNodes = 500;
			double alpha = 0.1;
			double lambda = 1;
			for (int run = 1; run < 10; run++)
			{
				alpha += 0.1;
				RandomStringReddyOrbanzGraph g = new RandomStringReddyOrbanzGraph(numNodes, alpha, lambda);
				assertEquals(g.getNumNodes(), numNodes);

				for (int resample = 0; resample < 100; resample++)
				{
					int totalEdges = 0;
					for (int i = 0; i < numNodes; i++)
					{
						int n = g.getNeighborCount(i);
						totalEdges += n;
					}
					if (alpha < 1)
						assert(g.getNumEdges() >= numNodes-1);
					else
						assert(g.getNumEdges() == numNodes-1);	
													
					g.resample();
					int newTotalEdges = 0;
					for (int i = 0; i < numNodes; i++)
						newTotalEdges += g.getNeighborCount(i);
					newTotalEdges >>= 1;	// Neighbors count edges twice
						assert(newTotalEdges != totalEdges);		// Make sure it isn't just the same graph (should be rare)
				}
			}
		}
	}

}
