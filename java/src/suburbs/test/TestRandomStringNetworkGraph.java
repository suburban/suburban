package suburbs.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import suburbs.RandomStringNetworkGraph;

import com.analog.lyric.math.DimpleRandomGenerator;

public class TestRandomStringNetworkGraph
{

	@Test
	public void test()
	{
		DimpleRandomGenerator.setSeed(12);

		{
			int numNodes = 4;
			double z = 0;
			RandomStringNetworkGraph g = new RandomStringNetworkGraph(numNodes, z);
			assertEquals(g.getNumNodes(), numNodes);
			for (int i = 0; i < numNodes; i++)
				assertEquals(g.getNeighborCount(i), 0);
			g.resample();
			assertEquals(g.getNumNodes(), numNodes);
			for (int i = 0; i < numNodes; i++)
				assertEquals(g.getNeighborCount(i), 0);
		}
		
		{
			int numNodes = 4;
			double z = 1;
			RandomStringNetworkGraph g = new RandomStringNetworkGraph(numNodes, z);
			assertEquals(g.getNumNodes(), numNodes);
			double[] pk = g.getDegreeDistribution();
			for (int i = 0; i < numNodes; i++)
				assertEquals(pk[i], 1./numNodes, 1e-10);
			g.resample();
		}


		{
			int numNodes = 500;
			double z = 0;
			for (int run = 1; run < 10; run++)
			{
				z += 0.1;
				RandomStringNetworkGraph g = new RandomStringNetworkGraph(numNodes, z);
				assertEquals(g.getNumNodes(), numNodes);
				
				double[] pk = g.getDegreeDistribution();
				double sum = 0;
				double degreeMean = 0;
				double degreeMeanSq = 0;
				for (int i = 0; i < numNodes; i++)
				{
					sum += pk[i];
					degreeMean += i * pk[i];
					degreeMeanSq += i*i * pk[i];
				}
				double degreeVar = degreeMeanSq - degreeMean*degreeMean;
				double degreeStd = Math.sqrt(degreeVar);
				assertEquals(sum, 1, 1e-10);
				assertEquals(degreeMean, z/(1-z), 1e-10);
				assertEquals(degreeVar, z/((1-z)*(1-z)), 1e-10);
				
				
				int[] edgeTotal = new int[numNodes];
				int[] edgeTotalSq = new int[numNodes];
				final int numResamples = 1000;
				for (int resample = 0; resample < numResamples; resample++)
				{
					for (int i = 0; i < numNodes; i++)
					{
						int n = g.getNeighborCount(i);
						edgeTotal[i] += n;
						edgeTotalSq[i] += n*n;
					}
					
					g.resample();
				}
				
				double[] estMean = new double[numNodes];
				double[] estVar = new double[numNodes];
				for (int i = 0; i < numNodes; i++)
				{
					estMean[i] = (double)edgeTotal[i]/(double)numResamples;
					estVar[i] = (double)edgeTotalSq[i]/(double)numResamples - estMean[i]*estMean[i];
				}
				
				double meanSum = 0;
				for (int i = 0; i < numNodes; i++)
					meanSum += estMean[i];
				double meanMean = meanSum/numNodes;
				
				assertEquals(degreeMean, meanMean, degreeStd / Math.sqrt(numNodes));
				for (int i = 0; i < numNodes; i++)
				{
					assertEquals(degreeMean, estMean[i], degreeStd);
					assert(estVar[i] > 1e-3);
				}
				
				sum = 0;	// Breakpoint target
			}
		}
	}

}
