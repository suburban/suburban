package suburbs.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.Set;

import org.junit.Test;

import suburbs.StringGraph;
import suburbs.StringGraph.Edge;

import com.analog.lyric.math.DimpleRandomGenerator;

public class TestStringGraph
{
	private final static double EPS = Math.ulp(1.0);

	@Test
	public void testSmall()
	{
		int numNodes = 3;
		StringGraph g = new StringGraph(numNodes);
		assertEquals(g.getNumNodes(), numNodes);
		assertEquals(g.getNumEdges(), 0);
		assertEquals(g.getEdges().size(), 0);
		assertEquals(g.getAverageDegree(), 0, EPS);
		for (int i = 0; i < numNodes; i++)
		{
			assertEquals(g.getNeighborCount(i), 0);
			assertEquals(g.getNeighbors(i).size(), 0);
		}

		g.addEdge(0, 2);
		assertEquals(g.getNumNodes(), numNodes);
		assertEquals(g.getNumEdges(), 1);
		assertEquals(g.getEdges().size(), 1);
		assert(g.getEdges().contains(new Edge(0, 2)));
		assert(g.getNeighbors(0).contains(2));
		assertEquals(g.getNeighbors(1).size(), 0);
		assert(g.getNeighbors(2).contains(0));
		assertEquals(g.getAverageDegree(),  2./3.,EPS);

		g.addEdge(new Edge(2, 1));
		assertEquals(g.getNumNodes(), numNodes);
		assertEquals(g.getNumEdges(), 2);
		assertEquals(g.getEdges().size(), 2);
		assert(g.getEdges().contains(new Edge(0, 2)));
		assert(g.getEdges().contains(new Edge(2, 1)));
		assertFalse(g.getEdges().contains(new Edge(1, 2)));
		assert(g.getNeighbors(0).contains(2));
		assert(g.getNeighbors(1).contains(2));
		assert(g.getNeighbors(2).contains(0));
		assert(g.getNeighbors(2).contains(1));
		assertEquals(g.getAverageDegree(),  4./3.,EPS);

		DimpleRandomGenerator.setSeed(24);
		g.reorderNodes();
		assertEquals(g.getNumNodes(), numNodes);
		assertEquals(g.getNumEdges(), 2);
		assertEquals(g.getEdges().size(), 2);
		assert(g.getEdges().contains(new Edge(1, 0)));
		assert(g.getEdges().contains(new Edge(0, 2)));
		assertFalse(g.getEdges().contains(new Edge(2, 0)));
		assert(g.getNeighbors(1).contains(0));
		assert(g.getNeighbors(2).contains(0));
		assert(g.getNeighbors(0).contains(1));
		assert(g.getNeighbors(0).contains(2));
		assertEquals(g.getAverageDegree(),  4./3.,EPS);
	}

	@Test
	public void testLarge()
	{
		for (int run = 0; run < 10; run++)
		{
			DimpleRandomGenerator.setSeed(run);
			int numNodes = DimpleRandomGenerator.rand.nextInt(20)+2;
			StringGraph g = new StringGraph(numNodes);
			assertEquals(g.getNumNodes(), numNodes);
			assertEquals(g.getNumEdges(), 0);
			assertEquals(g.getEdges().size(), 0);

			int numEdges = DimpleRandomGenerator.rand.nextInt(100)+2;
			int[][] edges = new int[numEdges][2];
			for (int i = 0; i < numEdges; i++)
			{
				int first = DimpleRandomGenerator.rand.nextInt(numNodes);
				int second = DimpleRandomGenerator.rand.nextInt(numNodes-1);
				if (second >= first)
					second++;
				g.addEdge(first, second);
				edges[i][0] = first;
				edges[i][1] = second;
			}
			int numUniqueEdges = numUnique(edges);
			assertEquals(g.getNumNodes(), numNodes);
			assertEquals(g.getNumEdges(), numUniqueEdges);
			checkNodeEdgeConsistency(g);

			for (int i = 0; i < 10; i++)
			{
				g.reorderNodes();
				checkNodeEdgeConsistency(g);
				assertEquals(g.getNumNodes(), numNodes);
				assertEquals(g.getNumEdges(), numUniqueEdges);
			}
		}
	}

	private void checkNodeEdgeConsistency(StringGraph g)
	{
		Set<Edge> gEdges = g.getEdges();
		for (int i = 0; i < g.getNumNodes(); i++)
			for (Integer n : g.getNeighbors(i))
				assert(gEdges.contains(new Edge(i,n)) || gEdges.contains(new Edge(n,i)));
		for (Edge e : gEdges)
			assert(g.getNeighbors(e.getFirst()).contains(e.getSecond()) && g.getNeighbors(e.getSecond()).contains(e.getFirst()));
	}

	private int numUnique(int[][] edges)
	{
		int numUnique = edges.length;
		for (int i = 0; i < edges.length; i++)
		{
			for (int j = i+1; j < edges.length; j++)
			{
				if (edges[i][0] == edges[j][0] && edges[i][1] == edges[j][1])
				{
					numUnique--;
					break;
				}
			}
		}
		return numUnique;
	}
}
