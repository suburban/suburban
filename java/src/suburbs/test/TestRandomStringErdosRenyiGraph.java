package suburbs.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import suburbs.RandomStringErdosRenyiGraph;

import com.analog.lyric.math.DimpleRandomGenerator;

public class TestRandomStringErdosRenyiGraph
{
	private final static double EPS = Math.ulp(1.0);

	@Test
	public void test()
	{
		DimpleRandomGenerator.setSeed(12);

		{
			int numNodes = 4;
			double p = 0;
			RandomStringErdosRenyiGraph g = new RandomStringErdosRenyiGraph(numNodes, p);
			assertEquals(g.getNumNodes(), numNodes);
			for (int i = 0; i < numNodes; i++)
				assertEquals(g.getNeighborCount(i), 0);
			g.resample();
			assertEquals(g.getNumNodes(), numNodes);
			for (int i = 0; i < numNodes; i++)
				assertEquals(g.getNeighborCount(i), 0);
		}

		{
			int numNodes = 4;
			double p = 1;
			RandomStringErdosRenyiGraph g = new RandomStringErdosRenyiGraph(numNodes, p);
			assertEquals(g.getNumNodes(), numNodes);
			for (int i = 0; i < numNodes; i++)
				assertEquals(g.getNeighborCount(i), numNodes-1);
			g.resample();
			assertEquals(g.getNumNodes(), numNodes);
			for (int i = 0; i < numNodes; i++)
				assertEquals(g.getNeighborCount(i), numNodes-1);
		}
		
		{
			int numNodes = 50;
			double p = 0;
			RandomStringErdosRenyiGraph g = new RandomStringErdosRenyiGraph(numNodes, p);
			assertEquals(g.getNumNodes(), numNodes);
			assertEquals(g.getAverageDegree(), 0, EPS);
			for (int run = 1; run < 1000; run++)
				g.resample();
			assertEquals(g.getAverageDegree(), 0, EPS);
			assertEquals(g.getAverageDegreeForAllSamples(), 0, EPS);
		}
		
		{
			int numNodes = 50;
			double p = 1;
			RandomStringErdosRenyiGraph g = new RandomStringErdosRenyiGraph(numNodes, p);
			assertEquals(g.getNumNodes(), numNodes);
			assertEquals(g.getAverageDegree(), numNodes-1, EPS);
			for (int run = 1; run < 1000; run++)
				g.resample();
			assertEquals(g.getAverageDegree(), numNodes-1, EPS);
			assertEquals(g.getAverageDegreeForAllSamples(), numNodes-1, EPS);
		}
		
		{
			int numNodes = 50;
			double p = 0.5;
			RandomStringErdosRenyiGraph g = new RandomStringErdosRenyiGraph(numNodes, p);
			assertEquals(g.getNumNodes(), numNodes);
			for (int run = 1; run < 10000; run++)
				g.resample();
			assertEquals(g.getAverageDegreeForAllSamples(), (double)(numNodes-1)/2., 0.01);
		}

		{
			int numNodes = 500;
			double p = 0;
			for (int run = 1; run < 10; run++)
			{
				p += 0.1;
				RandomStringErdosRenyiGraph g = new RandomStringErdosRenyiGraph(numNodes, p);
				assertEquals(g.getNumNodes(), numNodes);

				for (int resample = 0; resample < 10; resample++)
				{
					int totalEdges = 0;
					int maxNumEdges = numNodes*(numNodes-1)/2;
					double expectedTotalEdges = maxNumEdges * p;
					double expectedStdDev = Math.sqrt(maxNumEdges * p * (1-p));
					double nodeExpectedNeighbors = numNodes * p;
					double nodeStdDev = Math.sqrt(numNodes * p * (1-p));
					for (int i = 0; i < numNodes; i++)
					{
						int n = g.getNeighborCount(i);
						totalEdges += n;
						assert(Math.abs(n - nodeExpectedNeighbors) < nodeStdDev*5);
					}
					totalEdges >>= 1;	// Neighbors count edges twice
						assert(Math.abs(totalEdges - expectedTotalEdges) < expectedStdDev*3);
						
					g.resample();
					int newTotalEdges = 0;
					for (int i = 0; i < numNodes; i++)
						newTotalEdges += g.getNeighborCount(i);
					newTotalEdges >>= 1;	// Neighbors count edges twice
						assert(newTotalEdges != totalEdges);		// Make sure it isn't just the same graph (should be rare)
				}
			}
		}
	}

}
