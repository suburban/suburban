package suburbs.test;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import suburbs.RandomStringTree;

import com.analog.lyric.math.DimpleRandomGenerator;

public class TestRandomStringTree
{

	@Test
	public void test()
	{
		DimpleRandomGenerator.setSeed(12);

		{
			// Simple small graph that should be a tree (connectionProbability = 1)
			int numNodes = 4;
			int maxDegree = numNodes - 1;
			double connectionProbability = 1;
			RandomStringTree g = new RandomStringTree(numNodes, maxDegree, connectionProbability);
			assertEquals(g.getNumNodes(), numNodes);
			assertEquals(g.getNumEdges(), numNodes-1);
			g.resample();
			assertEquals(g.getNumNodes(), numNodes);
			assertEquals(g.getNumEdges(), numNodes-1);
		}
		
		{
			// Random-size graph that should be a tree (connectionProbability = 1)
			for (int i = 0; i < 100; i++)
			{
				int numNodes = DimpleRandomGenerator.rand.nextInt(100) + 5;
				int maxDegree = 2;
				double connectionProbability = 1;
				RandomStringTree g = new RandomStringTree(numNodes, maxDegree, connectionProbability);
				assertEquals(g.getNumNodes(), numNodes);
				assertEquals(g.getNumEdges(), numNodes-1);
				for (int n = 0; i < numNodes; i++)
					assert(g.getNeighborCount(n) <= maxDegree);
				g.resample();
				assertEquals(g.getNumNodes(), numNodes);
				assertEquals(g.getNumEdges(), numNodes-1);
				for (int n = 0; i < numNodes; i++)
					assert(g.getNeighborCount(n) <= maxDegree);
			}
		}

		{
			// Random-size graph that should be a tree (connectionProbability = 1)
			for (int i = 0; i < 100; i++)
			{
				int numNodes = DimpleRandomGenerator.rand.nextInt(100) + 5;
				int maxDegree = DimpleRandomGenerator.rand.nextInt(numNodes/10 + 1) + 2;
				double connectionProbability = 1;
				RandomStringTree g = new RandomStringTree(numNodes, maxDegree, connectionProbability);
				assertEquals(g.getNumNodes(), numNodes);
				assertEquals(g.getNumEdges(), numNodes-1);
				for (int n = 0; i < numNodes; i++)
					assert(g.getNeighborCount(n) <= maxDegree);
				g.resample();
				assertEquals(g.getNumNodes(), numNodes);
				assertEquals(g.getNumEdges(), numNodes-1);
				for (int n = 0; i < numNodes; i++)
					assert(g.getNeighborCount(n) <= maxDegree);
			}
		}

		{
			// Random-size graph that should be a forest (connectionProbability < 1)
			for (int i = 0; i < 100; i++)
			{
				int numNodes = DimpleRandomGenerator.rand.nextInt(100) + 5;
				int maxDegree = DimpleRandomGenerator.rand.nextInt(numNodes/10 + 1) + 2;
				double connectionProbability = 0.1;
				RandomStringTree g = new RandomStringTree(numNodes, maxDegree, connectionProbability);
				assertEquals(g.getNumNodes(), numNodes);
				assert(g.getNumEdges() < numNodes-1);
				for (int n = 0; i < numNodes; i++)
					assert(g.getNeighborCount(n) <= maxDegree);
				g.resample();
				assertEquals(g.getNumNodes(), numNodes);
				assert(g.getNumEdges() < numNodes-1);
				for (int n = 0; i < numNodes; i++)
					assert(g.getNeighborCount(n) <= maxDegree);
			}
		}

	}

}
