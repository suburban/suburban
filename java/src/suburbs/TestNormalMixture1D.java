package suburbs;

import com.analog.lyric.dimple.factorfunctions.core.FactorFunction;
import com.analog.lyric.dimple.model.values.Value;


public class TestNormalMixture1D extends FactorFunction
{
	protected double _mean_0 = 0;
	protected double _mean_1 = 0;
	protected double _logSqrtPrecisionOver2Pi_0;
	protected double _precisionOverTwo_0;
	protected double _logSqrtPrecisionOver2Pi_1;
	protected double _precisionOverTwo_1;
	protected static final double _logSqrt2pi = Math.log(2*Math.PI)*0.5;

	public TestNormalMixture1D(double mean_0, double precision_0, double mean_1, double precision_1)
	{
		super();
		_mean_0 = mean_0;
		_mean_1 = mean_1;		
		_precisionOverTwo_0 = precision_0*0.5;
		_precisionOverTwo_1 = precision_1*0.5;
		_logSqrtPrecisionOver2Pi_0 = Math.log(precision_0)*0.5 - _logSqrt2pi;
		_logSqrtPrecisionOver2Pi_1 = Math.log(precision_1)*0.5 - _logSqrt2pi;
	}
	
    @Override
    public final double evalEnergy(Value[] arguments)
    {
    	final double x = arguments[0].getDouble();
    	
    	final double xRel_0 = x - _mean_0;
    	final double e_0 = (xRel_0 * xRel_0) * _precisionOverTwo_0 - _logSqrtPrecisionOver2Pi_0;
    	final double xRel_1 = x - _mean_1;
    	final double e_1 = (xRel_1 * xRel_1) * _precisionOverTwo_1 - _logSqrtPrecisionOver2Pi_1;
		
    	final double min_e = (e_1 < e_0) ? e_1 : e_0;
    	final double e = min_e - Math.log(1 + Math.exp(-Math.abs(e_1 - e_0)));	
		return e;
    }
}
