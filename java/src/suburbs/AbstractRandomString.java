package suburbs;

import java.util.Set;

public abstract class AbstractRandomString implements IRandomString
{
	protected StringGraph _stringGraph;
	protected long _resampleCount;
	protected long _totalEdgeCount;
	
	public AbstractRandomString(int numNodes)
	{
		_stringGraph = new StringGraph(numNodes);
		_resampleCount = 0;
		_totalEdgeCount = 0;
	}
	
	public final int getNumNodes()
	{
		return _stringGraph.getNumNodes();
	}
	
	public final int getNumEdges()
	{
		return _stringGraph.getNumEdges();
	}
	
	public final int getNeighborCount(int elementIndex)
	{
		return _stringGraph.getNeighborCount(elementIndex);
	}
	
	public final Set<Integer> getNeighbors(int elementIndex)
	{
		return _stringGraph.getNeighbors(elementIndex);
	}
	
	public final int[][] getAdjacencyMatrix()
	{
		return _stringGraph.getAdjacencyMatrix();
	}
	
	public final double getAverageDegree()
	{
		return _stringGraph.getAverageDegree();
	}

	public final double getAverageDegreeForAllSamples()
	{
		return (double)(_totalEdgeCount) * 2. / (double)(_stringGraph.getNumNodes() * _resampleCount);
	}
	
	// To be called at the end of the resample method in derived classes
	protected final void postResample()
	{
		_resampleCount++;
		_totalEdgeCount += _stringGraph.getNumEdges();
	}
	
	// Abstract methods
	public abstract void resample();

}
