package suburbs;


import com.analog.lyric.dimple.exceptions.DimpleException;
import com.analog.lyric.dimple.factorfunctions.core.FactorFunction;
import com.analog.lyric.dimple.model.values.Value;


public class TestBananaPairwiseJoint extends FactorFunction
{
	protected double[] _centers;
	protected double[] _invwidths;
	protected int _dimension;

	public TestBananaPairwiseJoint(double[] centers, double[] invwidths)
	{
		super();
		_centers = centers;  // where you put the center of the gaussian part: (x - 1)^2
		_invwidths = invwidths; // the thing that controls the quartic term:  (x - y^2)^2
		_dimension = centers.length * 2;
	}
	
    @Override
    public final double evalEnergy(Value[] arguments)
    {
    	final double[] x = arguments[0].getDoubleArray();
		if (_dimension != x.length) throw new DimpleException("Number of dimensions must equal twice the number of elements in the parameter vectors");

		double energy = 0;
		
		final int dm1 = _dimension - 1;
    	for (int dcount = 0, icount = 0; dcount < dm1; dcount += 2, icount++) // compute the energy as a sum of terms...
    	{
    		final double xd = x[dcount];
    		final double firstdiff = xd - _centers[icount];
    		final double secondiff = x[dcount + 1] - xd * xd;
    		final double firstterm = firstdiff * firstdiff + _invwidths[icount] * (secondiff * secondiff);
    		energy += firstterm;
    	}
    	
    	return energy;  // eat the banana!
    }
}
