package suburbs;

import com.analog.lyric.dimple.model.core.FactorGraph;
import com.analog.lyric.dimple.solvers.core.SolverBase;

public class SuburbanSolver extends SolverBase<SuburbanSolverGraph>
{
	public SuburbanSolver()
	{
		super();
	}

	@Override
	public SuburbanSolverGraph createFactorGraph(FactorGraph graph)
	{
		return new SuburbanSolverGraph(graph);
	}
}
