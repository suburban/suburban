package suburbs;

import com.analog.lyric.dimple.model.domains.Domain;
import com.analog.lyric.dimple.model.values.Value;
import com.analog.lyric.dimple.solvers.core.proposalKernels.Proposal;

public interface ISuburbanString
{
	public Proposal getProposal(int updateIndexOffset, Value currentValue, Domain variableDomain);
}
