package suburbs;


import com.analog.lyric.dimple.exceptions.DimpleException;
import com.analog.lyric.dimple.factorfunctions.core.FactorFunction;
import com.analog.lyric.dimple.model.values.Value;


public class TestNormalMixtureJoint extends FactorFunction
{
	protected double[][] _means;
	protected double[][][] _informationMatrices;
	protected double[] _weights;
	protected double[] _diff;
	protected double[] _energies;
	protected int _dimension;
	protected int _numMixtureComponents;
	protected double[] _normalizationConstant;
	protected static final double _logSqrt2pi = Math.log(2*Math.PI)*0.5;

	public TestNormalMixtureJoint(double[][] means, double[][][] informationMatrices, double[] weights)
	{
		super();
		_means = means;
		_informationMatrices = informationMatrices;
		_weights = weights;
		_dimension = means[0].length;
		_numMixtureComponents = weights.length;
		_diff = new double[_dimension];
		_energies = new double[_numMixtureComponents];
		_normalizationConstant = new double[_numMixtureComponents];
		for (int m = 0; m < _numMixtureComponents; m++)
		{
			Jama.Matrix informationMatrix = new Jama.Matrix(_informationMatrices[m]);
			double determinant = informationMatrix.det();
			_normalizationConstant[m] = (_dimension * _logSqrt2pi) - (Math.log(determinant) * 0.5);
		}

	}
	
    @Override
    public final double evalEnergy(Value[] arguments)
    {
		final int N = _dimension;
    	final double[] x = arguments[0].getDoubleArray();
		if (_dimension != x.length) throw new DimpleException("Invalid variable dimension");


    	for (int mixtureComponent = 0; mixtureComponent < _numMixtureComponents; mixtureComponent++)
    	{
    		double sum = 0;
    		final double[] mean = _means[mixtureComponent];
    		final double[][] informationMatrix = _informationMatrices[mixtureComponent];
    		for (int i = 0; i < _dimension; i++)
    			_diff[i] = x[i] - mean[i];
    		double colSum = 0;
    		for (int row = 0; row < _dimension; row++)
    		{
    			double rowSum = 0;
    			final double[] informationMatrixRow = informationMatrix[row];
    			for (int col = 0; col < _dimension; col++)
    				rowSum += informationMatrixRow[col] * _diff[col];	// Matrix * vector
    			colSum += rowSum * _diff[row];	// Vector * vector
    		}
    		sum += colSum;
    		_energies[mixtureComponent] = sum * 0.5 + N * _normalizationConstant[mixtureComponent];
    	}

    	double minEnergy = Double.POSITIVE_INFINITY;
    	int minIndex = -1;
    	for (int m = 0; m < _numMixtureComponents; m++)
    	{
    		if (_energies[m] < minEnergy)
    		{
    			minEnergy = _energies[m];
    			minIndex = m;
    		}
    	}
    	
    	
    	double sum = 0;
    	double weightMin = _weights[minIndex];
    	for (int m = 0; m < _numMixtureComponents; m++)
    	{
    		sum += (_weights[m] / weightMin ) * Math.exp(minEnergy - _energies[m]);
    	}
    	
    	return -Math.log(sum) - Math.log(weightMin) + minEnergy;
    }
}
