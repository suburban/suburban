package suburbs;

import java.util.List;

import com.analog.lyric.dimple.model.domains.Domain;
import com.analog.lyric.dimple.model.values.Value;
import com.analog.lyric.dimple.solvers.core.proposalKernels.IProposalKernel;
import com.analog.lyric.dimple.solvers.core.proposalKernels.Proposal;
import com.analog.lyric.options.IOptionHolder;
import com.analog.lyric.options.Option;

public class SuburbanGeneralProposalKernel implements IProposalKernel
{
	private ISuburbanString _string;
	private int _variableIndex;

	public SuburbanGeneralProposalKernel(ISuburbanString string, int variableIndex)
	{
		_string = string;
		_variableIndex = variableIndex;
	}
	
	@Override
	public Proposal next(Value currentValue, Domain variableDomain)
	{
		return _string.getProposal(_variableIndex, currentValue, variableDomain);
	}

	
	
	@Override
	public void configureFromOptions(IOptionHolder optionHolder)
	{
	}
	@Override
	public List<Option<?>> getOptionConfiguration(List<Option<?>> list)
	{
		return null;
	}
	@Override
	public void setParameters(Object... parameters)
	{
	}
	@Override
	public Object[] getParameters()
	{
		return null;
	}
}
