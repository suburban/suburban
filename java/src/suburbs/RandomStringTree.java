package suburbs;

import java.util.ArrayList;
import java.util.Arrays;

import com.analog.lyric.dimple.exceptions.DimpleException;
import com.analog.lyric.math.DimpleRandomGenerator;

public class RandomStringTree extends AbstractRandomString
{
	private final int _maxDegree;
	private final double _connectionProbability;
	private final int[] _degree;
	

	public RandomStringTree(int numNodes, int maxDegree, double connectionProbability)
	{
		super(numNodes);
		if (maxDegree < 2)
			throw new DimpleException("maxDegree must be greater than one");
		if (connectionProbability < 0 || connectionProbability > 1)
			throw new DimpleException("connectionProbability must be between zero and one");
		_maxDegree = maxDegree;
		_connectionProbability = connectionProbability;
		_degree = new int[numNodes];
		resample();	// Generate initial graph
	}


	@Override
	public void resample()
	{
		final StringGraph stringGraph = _stringGraph;
		final int numNodes = stringGraph.getNumNodes();
		stringGraph.clear();
		
		Arrays.fill(_degree, 0);
		final int maxDegree = _maxDegree;
		final double connectionProbability = _connectionProbability;
		
		ArrayList<Integer> connectedNodes = new ArrayList<Integer>();
		ArrayList<Integer> unconnectedNodes = new ArrayList<Integer>();
		for (int i = 0; i < numNodes; i++)
			unconnectedNodes.add(i);
		
		for (int i = 0; i < numNodes; i++)
		{
			if (allMaxDegree(i))
			{
				// All connected nodes max degree, so choose a new one at random and add without connecting
				final int nextNode = unconnectedNodes.remove(DimpleRandomGenerator.rand.nextInt(unconnectedNodes.size()));
				connectedNodes.add(nextNode);
			}
			else
			{
				// Choose next node at random and add with probability of connecting
				final int nextNode = unconnectedNodes.remove(DimpleRandomGenerator.rand.nextInt(unconnectedNodes.size()));
		
				final double U = DimpleRandomGenerator.rand.nextDouble();
				if (U <= connectionProbability)
				{
					// Connect the new node to a randomly chosen existing node
					boolean nodeChosen = false;
					int node = -1;
					final int numConnected = connectedNodes.size();
					while (!nodeChosen)
					{
						// Choose a node already in the graph at random
						node = connectedNodes.get(DimpleRandomGenerator.rand.nextInt(numConnected));
						if (_degree[node] < maxDegree)
							nodeChosen = true;
					}
					stringGraph.addEdge(node, nextNode);
					_degree[node]++;
					_degree[nextNode]++;
				}
				connectedNodes.add(nextNode);
			}
		}
		
		super.postResample();
	}
	

	
	private final boolean allMaxDegree(int numToCheck)
	{
		final int maxDegree = _maxDegree;
		for (int i = 0; i < numToCheck; i++)
			if (_degree[i] < maxDegree)
				return false;
		return true;
	}
	
}
