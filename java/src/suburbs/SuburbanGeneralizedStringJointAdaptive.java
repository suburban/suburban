package suburbs;


import com.analog.lyric.dimple.model.domains.Domain;
import com.analog.lyric.dimple.model.values.RealJointValue;
import com.analog.lyric.dimple.model.values.Value;
import com.analog.lyric.dimple.solvers.core.proposalKernels.Proposal;
import com.analog.lyric.dimple.solvers.gibbs.GibbsRealJoint;
import com.analog.lyric.math.DimpleRandomGenerator;

public class SuburbanGeneralizedStringJointAdaptive implements ISuburbanString
{
	private final GibbsRealJoint[] _variables;
	private final IString _string;
	private final double _alpha_0;
	private final double _beta;
	private double[][] _neighborValues;
	private int _dimension;
	private static final double _logSqrt2pi = Math.log(2*Math.PI)*0.5;


	public SuburbanGeneralizedStringJointAdaptive(Object[] variables, IString string, double alpha_0, double beta)
	{
		_string = string;
		_alpha_0 = alpha_0;
		_beta = beta;

		_variables = new GibbsRealJoint[variables.length];
		for (int i = 0; i < variables.length; i++)
			_variables[i] = (GibbsRealJoint)variables[i];

		_dimension = _variables[0].getDimension();
		_neighborValues = new double[variables.length][_dimension];
	}
	
	public Proposal getProposal(int updateIndex, Value currentValue, Domain variableDomain)
	{		
		final int neighborCount = _string.getNeighborCount(updateIndex);
		final double beta = _beta;
		final double alpha = ((2 - neighborCount) * beta) + _alpha_0;
		final double precisionOverTwo = alpha + beta * neighborCount;
		final double meanNormalizationConstant = 1/precisionOverTwo;
		final double precision = 2*precisionOverTwo;
		final double sigma = 1/Math.sqrt(precision);
		final double logSqrtPrecisionOver2Pi = Math.log(precision)*0.5 - _logSqrt2pi;

		// Extract the current values
		final double[] current = currentValue.getDoubleArray();
		{
			int n = 0;
			for (Integer neighbor : _string.getNeighbors(updateIndex))
				_neighborValues[n++] = _variables[neighbor].getCurrentSample();
		}

		double proposalReverseEnergy = 0;
		double proposalForwardEnergy = 0;
		double[] proposal = new double[_dimension];
		for (int d = 0; d < _dimension; d++)
		{
			final double currentD = current[d];
			
			// Compute the pre-proposal mean
			double preMeanSum = alpha * currentD;
			for (int n = 0; n < neighborCount; n++)
				preMeanSum += beta * _neighborValues[n][d];
			final double preMean = preMeanSum * meanNormalizationConstant;

			// Generate the proposal to replace the current value
			double proposalValue = DimpleRandomGenerator.rand.nextGaussian() * sigma + preMean;
			proposal[d] = proposalValue;

			// Compute the post-proposal mean
			double postMeanSum = alpha * proposalValue;
			for (int n = 0; n < neighborCount; n++)
				postMeanSum += beta * _neighborValues[n][d];
			final double postMean = postMeanSum * meanNormalizationConstant;

			// Compute the Hastings term
			proposalReverseEnergy += -logQ(currentD, postMean, precisionOverTwo, logSqrtPrecisionOver2Pi);
			proposalForwardEnergy += -logQ(proposalValue, preMean, precisionOverTwo, logSqrtPrecisionOver2Pi);
		}

		return new Proposal(RealJointValue.create(proposal), proposalForwardEnergy, proposalReverseEnergy);
	}

	private final double logQ(double x, double mean, double precisionOverTwo, double logSqrtPrecisionOver2Pi)
	{
		double xRel = x - mean;
		return logSqrtPrecisionOver2Pi - (xRel * xRel) * precisionOverTwo;
	}
	
}
