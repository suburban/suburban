package suburbs;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.analog.lyric.math.DimpleRandomGenerator;
import com.analog.lyric.math.Utilities;

/**
 * 
 * Introduce a probability p_k to have degree k at a given vertex. For a graph with M vertices (numNodes), I propose to take:
 * 
 * p_k = z^k * (1 - z) / (1 - z^M)
 * 
 * where:
 * 
 * 0 <= k <= (M-1)
 * 0 <= z <= 1. 
 * 
 * z = 0 corresponds to parallel MH. z = 1 corresponds to p_k = 1/M, i.e. uniform odds of a given connectivity degree (0 to M-1).
 *
 */
public class RandomStringNetworkGraph extends AbstractRandomString
{
	private final double[] _unnormalizedDegreeDistribution;
	private final Random _rand;
	
	
	public RandomStringNetworkGraph(int numNodes, double z)
	{
		super(numNodes);
		_unnormalizedDegreeDistribution = new double[numNodes];
		double logZ = Math.log(z);
		if (logZ == Double.NEGATIVE_INFINITY)
		{
			_unnormalizedDegreeDistribution[0] = 1;
		}
		else
		{
			for (int k = 0; k < numNodes; k++)
				_unnormalizedDegreeDistribution[k] = Math.exp(logZ * k);
		}
		_rand = new Random(DimpleRandomGenerator.rand.nextLong());	// Use DimpleRandomGenerator as seed to allow repeatability
		resample();	// Generate initial graph
	}


	@Override
	public void resample()
	{
		final StringGraph stringGraph = _stringGraph;
		final int numNodes = stringGraph.getNumNodes();
		stringGraph.clear();
		
		ArrayList<Integer> stubList = new ArrayList<Integer>();

		for (int node = 0; node < numNodes; node++)
		{
			int k = Utilities.sampleFromMultinomial(_unnormalizedDegreeDistribution, _rand);
			for (int i = 0; i < k; i++)
				stubList.add(node);
		}
			
		while (stubList.size() > 1 && !allEqual(stubList))	// Make sure not left with *only* stubs from a single node
		{
			int a = DimpleRandomGenerator.rand.nextInt(stubList.size());
			int nodeA = stubList.get(a);
			stubList.remove(a);

			int b = DimpleRandomGenerator.rand.nextInt(stubList.size());
			int nodeB = stubList.get(b);
			stubList.remove(b);

			if (nodeA == nodeB)		// Can't add edge to/from same node
			{
				stubList.add(nodeA);
				stubList.add(nodeB);
				continue;
			}

			_stringGraph.addEdge(nodeA, nodeB);
		}
		
		super.postResample();
	}
	
	private final boolean allEqual(List<Integer> list)
	{
		int listLength = list.size();
		if (listLength <= 1)
			return true;
		
		int first = list.get(0);
		for (int i = 1; i < listLength; i++)
			if (list.get(i) != first)
				return false;
		
		return true;
	}
	
	public double[] getDegreeDistribution()
	{
		double[] normalized = new double[_unnormalizedDegreeDistribution.length];
		
		double sum = 0;
		for (int i = 0; i < _unnormalizedDegreeDistribution.length; i++)
			sum += _unnormalizedDegreeDistribution[i];
		
		for (int i = 0; i < _unnormalizedDegreeDistribution.length; i++)
			normalized[i] = _unnormalizedDegreeDistribution[i]/sum;
		
		return normalized;
	}

}
