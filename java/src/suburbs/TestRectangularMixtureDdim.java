package suburbs;


import com.analog.lyric.dimple.factorfunctions.core.FactorFunction;
import com.analog.lyric.dimple.model.values.Value;


public class TestRectangularMixtureDdim extends FactorFunction
{
	protected double[][] _means;
	protected double[][] _widths;
	protected double[] _x;
	protected int _dimension;
	protected int _numMixtureComponents;
	private double _highEnergy;
	private double _rampRate;

	
	public TestRectangularMixtureDdim(double[][] means, double[][] widths)
	{
		this(means, widths, 10000, 0);
	}
	public TestRectangularMixtureDdim(double[][] means, double[][] widths, double highEnergy)
	{
		this(means, widths, highEnergy, 0);
	}
	public TestRectangularMixtureDdim(double[][] means, double[][] widths, double highEnergy, double rampRate)
	{
		super();
		_means = means;
		_widths = widths;
		_dimension = means[0].length;
		_numMixtureComponents = means.length;
		_x = new double[_dimension];
		_highEnergy = highEnergy;
		_rampRate = rampRate;
	}
	
    @Override
    public final double evalEnergy(Value[] arguments)
    {
		for (int i = 0; i < _dimension; i++)
	    	_x[i] = arguments[i].getDouble();				// Input vector

		double nearestDistanceSquared = Double.POSITIVE_INFINITY;
		for (int mixtureComponent = 0; mixtureComponent < _numMixtureComponents; mixtureComponent++)
    	{
    		final double[] means = _means[mixtureComponent];
    		final double[] widths = _widths[mixtureComponent];
    		
    		boolean inRectangle = true;
    		double distanceSquared = 0;
    		for (int dimension = 0; dimension < _dimension; dimension++)
    		{
    			double x = _x[dimension];
    			final double mean = means[dimension];
    			final double halfWidth = widths[dimension] / 2;
    			final double lower = mean - halfWidth;
    			final double upper = mean + halfWidth;
    			if (x < lower || x > upper)
    			{
    				inRectangle = false;
    				
    				// Get distance to rectangle
    				if (_rampRate > 0)
    				{
    					double distance;
    					if (x < lower)
    						distance = lower - x;
    					else
    						distance = x - upper;
    					distanceSquared += distance*distance;
    				}
    			}
    		}
    		
    		if (_rampRate > 0)
    			if (distanceSquared < nearestDistanceSquared)
    				nearestDistanceSquared = distanceSquared;

    		// In one of the rectangles
    		if (inRectangle)
	    		return 0;
    	}
    	
    	// Not in any rectangle
		return _highEnergy + ((_rampRate > 0) ? (Math.sqrt(nearestDistanceSquared) * _rampRate) : 0);
    }
}
