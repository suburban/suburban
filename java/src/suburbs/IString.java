package suburbs;

import java.util.Set;

public interface IString
{
	public int getNumNodes();
	public int getNeighborCount(int elementIndex);
	public Set<Integer> getNeighbors(int elementIndex);
}
