package suburbs;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.math3.util.Pair;

import com.analog.lyric.math.DimpleRandomGenerator;

public class StringGraph implements IString
{
	private Set<Edge> _edges;
	private List<Set<Integer>> _neighbors;
	private final int[] _order;
	private final int _numNodes;
	
	public StringGraph(int numNodes)
	{
		_numNodes = numNodes;
		_edges = new HashSet<Edge>();
		_neighbors = new ArrayList<Set<Integer>>();
		for (int i = 0; i < numNodes; i++)
			_neighbors.add(new HashSet<Integer>());
		_order = new int[_numNodes];
		for (int i = 0; i < _numNodes; i++)
			_order[i] = i;
	}
	
	public void clear()
	{
		_edges.clear();
		for (int i = 0; i < _neighbors.size(); i++)
			_neighbors.get(i).clear();
	}
	
	public void addEdge(int elementIndex, int neighborIndex)
	{
		_neighbors.get(elementIndex).add(neighborIndex);
		_neighbors.get(neighborIndex).add(elementIndex);
		_edges.add(new Edge(elementIndex, neighborIndex));
	}
	public void addEdge(Edge edge)
	{
		int first = edge.getFirst();
		int second = edge.getSecond();
		_neighbors.get(first).add(second);
		_neighbors.get(second).add(first);
		_edges.add(edge);
	}
	
	public int getNumNodes()
	{
		return _numNodes;
	}
	
	public int getNumEdges()
	{
		return _edges.size();
	}
	
	public int getNeighborCount(int elementIndex)
	{
		return _neighbors.get(elementIndex).size();
	}
	
	public Set<Edge> getEdges()
	{
		return _edges;
	}
	
	public Set<Integer> getNeighbors(int elementIndex)
	{
		return _neighbors.get(elementIndex);
	}
	
	public int[][] getAdjacencyMatrix()
	{
		int[][] out = new int[_numNodes][_numNodes];
		for (Edge e : _edges)
		{
			int i = e.getFirst();
			int j = e.getSecond();
			out[i][j] = 1;
			out[j][i] = 1;
		}
		
		return out;
	}
	
	public double getAverageDegree()
	{
		return (double)(_edges.size() << 1) / (double)_numNodes;
	}
		
	
	public void reorderNodes()
	{
		shuffleOrder();
		
		Set<Edge> newEdges = new HashSet<Edge>();
		for (Edge edge : _edges)
			newEdges.add(new Edge(_order[edge.getFirst()], _order[edge.getSecond()]));
		_edges = newEdges;
		
		List<Set<Integer>> newNeighbors = new ArrayList<Set<Integer>>();
		for (int i = 0; i < _numNodes; i++)
			newNeighbors.add(new HashSet<Integer>());
		for (int i = 0; i < _numNodes; i++)
		{
			HashSet<Integer> newNeighborSet = new HashSet<Integer>();
			for (Integer neighbor : _neighbors.get(i))
				newNeighborSet.add(_order[neighbor]);
			newNeighbors.set(_order[i], newNeighborSet);
		}
		_neighbors = newNeighbors;
	}
	private final void shuffleOrder()
	{
		// Randomize the sequence
		final int numNodes = _numNodes;
		for (int i = numNodes - 1; i > 0; i--)
		{
			final int randRange = i + 1;
		    final int randIndex = DimpleRandomGenerator.rand.nextInt(randRange);
		    final int nextIndex = _order[randIndex];
		    _order[randIndex] = _order[i];
		    _order[i] = nextIndex;
		}
	}
	
	public static class Edge extends Pair<Integer,Integer>
	{
		public Edge(Integer k, Integer v)
		{
			super(k, v);
		}
		
		public String toString()
		{
			return "[" + getFirst().toString() + ", " + getSecond().toString() + "]";
		}
	}
		
}
